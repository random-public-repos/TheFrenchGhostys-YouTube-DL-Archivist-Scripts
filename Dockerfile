FROM python:alpine

RUN apk add --no-cache \
  ffmpeg \
  tzdata

WORKDIR /ytarchive
COPY requirements.txt /ytarchive/
RUN pip install --no-cache-dir -r requirements.txt

COPY entrypoint.sh /
ENTRYPOINT ["/entrypoint.sh"]
